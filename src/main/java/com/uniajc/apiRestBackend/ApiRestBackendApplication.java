package com.uniajc.apiRestBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRestBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestBackendApplication.class, args);
	}

}
