package com.uniajc.apiRestBackend.rest;


import com.uniajc.apiRestBackend.model.items;
import com.uniajc.apiRestBackend.repo.IitemsRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/items")
@Api(value="Sistema administrativo de items", description="Operaciones habilitadas para items en el sistema administrativo")
public class RestitemsController {
	
	@Autowired
	private IitemsRepo repo;
	
	@GetMapping
	@ApiOperation(value = "Listado de items del sistema SAP", response = items.class)
	public List<items> listar(){
		return repo.findAll();
	}
	
	@GetMapping(value = "/{id}")
	@ApiOperation(value = "Listado de items del sistema", response = items.class)
	public java.util.Optional<items> mostrar(@PathVariable("id") Integer id){
		return repo.findById(id);
	}
	
	@PostMapping
	@ApiOperation(value = "Crear un nuevo item")
	public ResponseEntity<items> insertar(@RequestBody items item){
		repo.save(item);
		
		return new ResponseEntity<>(
				item, 
		      HttpStatus.OK);
	}
	
	@PutMapping
	@ApiOperation(value = "Actualizar un item")
	public ResponseEntity<items> modificar(@RequestBody items item){
		repo.save(item);
		
		return new ResponseEntity<>(
				item, 
		      HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}")
	@ApiOperation(value = "Borrar un item")
	public void eliminar(@PathVariable("id") Integer id){
		repo.deleteById(id);
	}
}
