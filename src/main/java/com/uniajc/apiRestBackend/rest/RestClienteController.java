package com.uniajc.apiRestBackend.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uniajc.apiRestBackend.model.Cliente;
import com.uniajc.apiRestBackend.repo.IClienteRepo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/clientes")
@Api(value="Sistema administrativo de Clientes", description="Operaciones habilitadas para Clientes en el sistema administrativo")
public class RestClienteController {
	@Autowired
	private IClienteRepo repo;
	
	@GetMapping
	@ApiOperation(value = "Listado de Clientes del sistema SAP", response = Cliente.class)
	public List<Cliente> listar(){
		return repo.findAll();
	}
	
	/*@GetMapping(value = "/{id}")
	@ApiOperation(value = "Listado de clientes del sistema", response = Cliente.class)
	public Optional<Cliente> mostrar(@PathVariable("id") Integer id){
		return repo.findById(id);
	}*/
	
	@GetMapping(value = "/{identificacion}")
	@ApiOperation(value = "Listado de clientes del sistema", response = Cliente.class)
	public ResponseEntity <Cliente> mostrarporidentificacion(@PathVariable("identificacion") String identificacion){
		Cliente cliente = repo.findByIdentificacion(identificacion);
		
		return new ResponseEntity<>(
				cliente, 
		      HttpStatus.OK);
	}
	
	@PostMapping
	@ApiOperation(value = "Crear un nuevo cliente")
	public ResponseEntity <Cliente> insertar(@RequestBody Cliente cliente){
		repo.save(cliente);
		
		return new ResponseEntity<>(
					cliente, 
			      HttpStatus.OK);
	}
	
	@PutMapping
	@ApiOperation(value = "Actualizar un cliente")
	public void modificar(@RequestBody Cliente cliente){
		repo.save(cliente);
	}
	
	/*@DeleteMapping(value = "/{id}")
	@ApiOperation(value = "Borrar una Persona")
	public void eliminar(@PathVariable("id") Integer id){
		repo.deleteById(id);
	}*/
}
