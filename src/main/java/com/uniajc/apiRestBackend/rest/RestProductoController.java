package com.uniajc.apiRestBackend.rest;


import com.uniajc.apiRestBackend.model.Producto;
import com.uniajc.apiRestBackend.repo.IProductoRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/productos")
@Api(value="Sistema administrativo de Productos", description="Operaciones habilitadas para productos en el sistema administrativo")
public class RestProductoController {
	
	@Autowired
	private IProductoRepo repo;
	
	@GetMapping
	@ApiOperation(value = "Listado de productos del sistema SAP", response = Producto.class)
	public List<Producto> listar(){
		return repo.findAll();
	}
	
	@GetMapping(value = "/{id}")
	@ApiOperation(value = "Listado de productos del sistema", response = Producto.class)
	public java.util.Optional<Producto> mostrar(@PathVariable("id") Integer id){
		return repo.findById(id);
	}
	
	@PostMapping
	@ApiOperation(value = "Crear un nuevo producto")
	public ResponseEntity<Producto> insertar(@RequestBody Producto producto){
		repo.save(producto);
		
		return new ResponseEntity<>(
				producto, 
		      HttpStatus.OK);
	}
	
	@PutMapping
	@ApiOperation(value = "Actualizar un producto")
	public ResponseEntity<Producto> modificar(@RequestBody Producto producto){
		repo.save(producto);
		
		return new ResponseEntity<>(
				producto, 
		      HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}")
	@ApiOperation(value = "Borrar un producto")
	public void eliminar(@PathVariable("id") Integer id){
		repo.deleteById(id);
	}
}
