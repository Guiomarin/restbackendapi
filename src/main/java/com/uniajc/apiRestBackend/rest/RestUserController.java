package com.uniajc.apiRestBackend.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uniajc.apiRestBackend.model.User;
import com.uniajc.apiRestBackend.model.UserLogin;
import com.uniajc.apiRestBackend.repo.IUserRepo;
import com.uniajc.apiRestBackend.security.JwtGenerator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/usuarios")
@Api(value="Sistema administrativo de Usuario", description="Operaciones habilitadas para Usuarios en el sistema administrativo")
public class RestUserController {

	@Autowired
	private IUserRepo repo;
	
	@Autowired
	private JwtGenerator jwtGenerator;
	
	@GetMapping
	@ApiOperation(value = "Listado de Usuarios del sistema SAP", response = User.class)
	public List<User> listar(){
		return repo.findAll();
	}
	
	@PostMapping("/create")
	@ApiOperation(value = "Crear un nuevo Usuario")
	public ResponseEntity<User> insertar(@RequestBody User user){
		String token = jwtGenerator.generate(user);
		user.setJwt(token);
		repo.save(user);
		
		return new ResponseEntity<>(
				user, 
		      HttpStatus.OK);
	}
	
	@PutMapping("/update")
	@ApiOperation(value = "Actualizar un Usuario")
	public ResponseEntity<User> modificar(@RequestBody User user){
		repo.save(user);
		
		return new ResponseEntity<>(
				user, 
		      HttpStatus.OK);
	}
	
	
	@PostMapping("/login")
	@ApiOperation(value = "Logearse en el sistema")
	public ResponseEntity<User> login(@RequestBody UserLogin user){
		User usuario = repo.findByUsername(user.getUsername());
		System.out.println(usuario.getPassword());
		System.out.println(user.getPassword());
		if(usuario.getPassword().equals(user.getPassword()) ) {
			return new ResponseEntity<>(
				      usuario, 
				      HttpStatus.OK);
		}else {
			return new ResponseEntity<>(
				      null, 
				      HttpStatus.UNAUTHORIZED);
		}
		
		
	}
	
	
}
