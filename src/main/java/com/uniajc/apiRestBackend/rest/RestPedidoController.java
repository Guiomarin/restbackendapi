package com.uniajc.apiRestBackend.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uniajc.apiRestBackend.model.Pedido;
import com.uniajc.apiRestBackend.repo.IPedidoRepo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/pedidos")
@Api(value="Sistema administrativo de Pedidos", description="Operaciones habilitadas para Pedidos en el sistema administrativo")
public class RestPedidoController {
	
	@Autowired
	private IPedidoRepo repo;
	
	@GetMapping
	@ApiOperation(value = "Listado de Pedidos del sistema SAP", response = Pedido.class)
	public List<Pedido> listar(){
		return repo.findAll();
	}
	
	
	@PostMapping
	@ApiOperation(value = "Crear un nuevo pedido")
	public ResponseEntity<Pedido> insertar(@RequestBody Pedido pedido){
		repo.save(pedido);
		
		return new ResponseEntity<>(
				pedido, 
		      HttpStatus.OK);
	}
	
}