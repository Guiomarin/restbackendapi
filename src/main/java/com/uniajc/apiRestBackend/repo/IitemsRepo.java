package com.uniajc.apiRestBackend.repo;


import com.uniajc.apiRestBackend.model.items;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IitemsRepo extends JpaRepository<items, Integer> {

}
