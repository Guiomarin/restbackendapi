package com.uniajc.apiRestBackend.repo;

import com.uniajc.apiRestBackend.model.Pedido;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPedidoRepo extends JpaRepository<Pedido, Integer> {

}
