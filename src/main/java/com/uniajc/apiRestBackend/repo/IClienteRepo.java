package com.uniajc.apiRestBackend.repo;


import com.uniajc.apiRestBackend.model.Cliente;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IClienteRepo extends JpaRepository<Cliente, Integer> {
	
	Cliente findByIdentificacion(String identificacion);
}
