package com.uniajc.apiRestBackend.repo;


import com.uniajc.apiRestBackend.model.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPersonaRepo extends JpaRepository<Persona, Integer> {

}
