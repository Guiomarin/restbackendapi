package com.uniajc.apiRestBackend.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@Table(name = "clientes")
@ApiModel(description = "Todos los detalles del modelo Cliente. ")
public class Cliente {
	@Id
	@ApiModelProperty(notes = "La base de Datos Genera automaticamente el ID de la persona")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "identificacion", length = 100)
	@ApiModelProperty(notes = "Identificacion del Cliente")
	private String identificacion;
	
	@Column(name = "nombrecompleto", length = 100)
	@ApiModelProperty(notes = "Nombre del Cliente")
	private String nombrecompleto;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getNombrecompleto() {
		return nombrecompleto;
	}

	public void setNombrecompleto(String nombrecompleto) {
		this.nombrecompleto = nombrecompleto;
	}
	
	
	
}
