package com.uniajc.apiRestBackend.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@Table(name = "productos")
@ApiModel(description = "Todos los detalles del modelo Producto. ")
public class Producto {
	@Id
	@ApiModelProperty(notes = "La base de Datos Genera automaticamente el ID del producto")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idProducto;
	
	@Column(name = "nombre", length = 100)
	@ApiModelProperty(notes = "Nombre(s) del Producto")
	private String nombre;
	
	@Column(name = "descripcion", length = 100)
	@ApiModelProperty(notes = "Descripcion(s) del producto")
	private String descripcion;
	
	@Column(name = "via", length = 100)
	@ApiModelProperty(notes = "Via de administracion")
	private String via;
	
	@Column(name = "medida", length = 100)
	@ApiModelProperty(notes = "Medida de administracion")
	private String medida;
		
	@Column(name = "cantidad", length = 100)
	@ApiModelProperty(notes = "Cantidad de administracion")
	private String cantidad;
	
	@Column(name = "unidad", length = 100)
	@ApiModelProperty(notes = "Unidad de administracion")
	private String unidad;
	
	@Column(name = "forma", length = 100)
	@ApiModelProperty(notes = "Forma")
	private String forma;

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getVia() {
		return via;
	}

	public void setVia(String via) {
		this.via = via;
	}

	public String getMedida() {
		return medida;
	}

	public void setMedida(String medida) {
		this.medida = medida;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public String getForma() {
		return forma;
	}

	public void setForma(String forma) {
		this.forma = forma;
	}

	@Override
	public String toString() {
		return "Producto [idProducto=" + idProducto + ", nombre=" + nombre + ", descripcion=" + descripcion + ", via="
				+ via + ", medida=" + medida + ", cantidad=" + cantidad + ", unidad=" + unidad + ", forma=" + forma
				+ ", getIdProducto()=" + getIdProducto() + ", getNombre()=" + getNombre() + ", getDescripcion()="
				+ getDescripcion() + ", getVia()=" + getVia() + ", getMedida()=" + getMedida() + ", getCantidad()="
				+ getCantidad() + ", getUnidad()=" + getUnidad() + ", getForma()=" + getForma() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	
	
}
