package com.uniajc.apiRestBackend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "pedidos")
@ApiModel(description = "Todos los detalles del modelo Pedido. ")
public class Pedido {
	
	@Id
	@ApiModelProperty(notes = "La base de Datos Genera automaticamente el ID de la persona")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "fecha", length = 100)
	@ApiModelProperty(notes = "Fecha del Pedido")
	private String fecha;
	
	@Column(name = "cliente_id")
	@ApiModelProperty(notes = "Cliente")
	private int cliente_id;
	
	@Column(name = "colaborador_id")
	@ApiModelProperty(notes = "Cliente")
	private int colaborador_id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getCliente_id() {
		return cliente_id;
	}

	public void setCliente_id(int cliente_id) {
		this.cliente_id = cliente_id;
	}

	public int getColaborador_id() {
		return colaborador_id;
	}

	public void setColaborador_id(int colaborador_id) {
		this.colaborador_id = colaborador_id;
	}

	@Override
	public String toString() {
		return "Pedido [id=" + id + ", fecha=" + fecha + ", cliente_id=" + cliente_id + ", colaborador_id="
				+ colaborador_id + "]";
	}
	
	
	
}
