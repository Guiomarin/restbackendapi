package com.uniajc.apiRestBackend.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@Table(name = "items")
@ApiModel(description = "Todos los detalles del modelo items. ")
public class items {
	@Id
	@ApiModelProperty(notes = "La base de Datos Genera automaticamente el ID del item")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
			
	@Column(name = "cantidad", length = 100)
	@ApiModelProperty(notes = "Cantidad")
	private int cantidad;
	
	@Column(name = "pedido_id", length = 100)
	@ApiModelProperty(notes = "Pedido")
	private int pedido_id;

	@Column(name = "medicamento_id", length = 100)
	@ApiModelProperty(notes = "Medicamento")
	private int medicamento_id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getPedido_id() {
		return pedido_id;
	}

	public void setPedido_id(int pedido_id) {
		this.pedido_id = pedido_id;
	}

	public int getMedicamento_id() {
		return medicamento_id;
	}

	public void setMedicamento_id(int medicamento_id) {
		this.medicamento_id = medicamento_id;
	}

	@Override
	public String toString() {
		return "items [id=" + id + ", cantidad=" + cantidad + ", pedido_id=" + pedido_id + ", medicamento_id="
				+ medicamento_id + "]";
	}
	
	
		
}
