package com.uniajc.apiRestBackend.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@Table(name = "colaboradores")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;
    
    @ApiModelProperty(notes = "Nombre Completo del Colaborador")
    @Column(name = "nombrecompleto")
    private String nombrecompleto;

    @ApiModelProperty(notes = "Usuario del colaborador")
    @Column(name = "usuario")
    private String username;

    @ApiModelProperty(notes = "Email del colaborador")
    @Column(name = "email")
    private String email;

    @ApiModelProperty(notes = "password of the User")
    @Column(name = "password")
    private String password;

    @ApiModelProperty(notes = "Token for security requests")
    @Column(name = "jwt")
    private String jwt;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombrecompleto() {
		return nombrecompleto;
	}

	public void setNombrecompleto(String nombrecompleto) {
		this.nombrecompleto = nombrecompleto;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String usuario) {
		this.username = usuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getJwt() {
		return jwt;
	}

	public void setJwt(String jwt) {
		this.jwt = jwt;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", nombrecompleto=" + nombrecompleto + ", username=" + username + ", email=" + email
				+ ", password=" + password + ", jwt=" + jwt + "]";
	}

   


}
