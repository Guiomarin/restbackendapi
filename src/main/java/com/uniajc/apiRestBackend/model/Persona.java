package com.uniajc.apiRestBackend.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@Table(name = "personas")
@ApiModel(description = "Todos los detalles del modelo Persona. ")
public class Persona {
	@Id
	@ApiModelProperty(notes = "La base de Datos Genera automaticamente el ID de la persona")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idPersona;
	
	@Column(name = "nombre", length = 100)
	@ApiModelProperty(notes = "Nombre(s) de la Persona")
	private String nombre;
	
	@Column(name = "apellido", length = 100)
	@ApiModelProperty(notes = "Apellido(s) de la persona")
	private String apellido;
	
	@Column(name = "documento", length = 100)
	@ApiModelProperty(notes = "Numero de Documento de la persona")
	private String documento;
	
	

	public int getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	@Override
	public String toString() {
		return "Persona [idPersona=" + idPersona + ", nombre=" + nombre + ", apellido=" + apellido + ", documento="
				+ documento + "]";
	}
	
	
	
}
