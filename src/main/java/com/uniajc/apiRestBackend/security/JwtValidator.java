package com.uniajc.apiRestBackend.security;

import com.uniajc.apiRestBackend.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;

@Component
public class JwtValidator {

    User validate(String token) {

        User jwtUser = null;
        try {
            String secret = "TokenUniajc";
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();

            jwtUser = new User();

            jwtUser.setUsername(body.getSubject());
            jwtUser.setId(Long.parseLong((String) body.get("id")));
            jwtUser.setEmail((String) body.get("email"));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return jwtUser;
    }
}
